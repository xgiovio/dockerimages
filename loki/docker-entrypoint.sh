#!/bin/bash


if [[ (! -z "${LOKI_DAEMON_USER}" ) && ( "${LOKI_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $LOKI_DAEMON_USER
	if [[ ! -z "${LOKI_DAEMON_USER_UID}" ]]; then
		usermod -u $LOKI_DAEMON_USER_UID $LOKI_DAEMON_USER
	fi
	if [[ ! -z "${LOKI_DAEMON_USER_GID}" ]]; then
		groupmod -g $LOKI_DAEMON_USER_GID $LOKI_DAEMON_USER
	fi
fi

if [ ! -d "/lokidata" ]; then
	mkdir -p /lokidata
fi

echo "Chowning Data"
if [[ ! -z "${LOKI_DAEMON_USER}" ]]; then
	chown -R $(id -u ${LOKI_DAEMON_USER}):$(id -g ${LOKI_DAEMON_USER}) /loki
	chown -R $(id -u ${LOKI_DAEMON_USER}):$(id -g ${LOKI_DAEMON_USER}) /lokidata
else
	chown -R $(id -u):$(id -g) /loki
	chown -R $(id -u):$(id -g) /lokidata
fi


if [[ ! -z "${LOKI_DAEMON_USER}" ]]; then
	runuser -u ${LOKI_DAEMON_USER} -- /loki/loki-linux-amd64 -config.file=/loki/config.yaml
else
	/loki/loki-linux-amd64 -config.file=/loki/config.yaml	
fi
