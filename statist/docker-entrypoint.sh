#!/bin/bash

if [[ (! -z "${NODE_DAEMON_USER}" ) && ( "${NODE_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $NODE_DAEMON_USER
	if [[ ! -z "${NODE_DAEMON_USER_UID}" ]]; then
		usermod -u $NODE_DAEMON_USER_UID $NODE_DAEMON_USER
	fi
	if [[ ! -z "${NODE_DAEMON_USER_GID}" ]]; then
		groupmod -g $NODE_DAEMON_USER_GID $NODE_DAEMON_USER
	fi
fi

echo "Chowning Data"
if [[ ! -z "${NODE_DAEMON_USER}" ]]; then
	chown -R $(id -u ${NODE_DAEMON_USER}):$(id -g ${NODE_DAEMON_USER}) /statist
else
	chown -R $(id -u):$(id -g) /statist
fi


if [[ ! -z "${NODE_DAEMON_USER}" ]]; then
	setcap 'cap_net_bind_service=+ep' /usr/bin/node
	runuser -u ${NODE_DAEMON_USER} -- $@
else
	$@	
fi
