#!/bin/bash


if [[ (! -z "${ADDITIONAL_USER}" ) && ( "${ADDITIONAL_USER}" != "root"  ) ]]; then
	useradd -m -s /bin/bash $ADDITIONAL_USER
	if [[ ! -z "${ADDITIONAL_USER_UID}" ]]; then
		usermod -u $ADDITIONAL_USER_UID $ADDITIONAL_USER
	fi
	if [[ ! -z "${ADDITIONAL_USER_GID}" ]]; then
		groupmod -g $ADDITIONAL_USER_GID $ADDITIONAL_USER
	fi
	if [[ ! -z "${ADDITIONAL_USER_PASSWORD_FILE}" ]]; then
		echo "${ADDITIONAL_USER}:$(cat $ADDITIONAL_USER_PASSWORD_FILE)" | chpasswd
	fi
	
	if [[ ! -z "${ADDITIONAL_USER_PUBLICKEY}" ]]; then
		if [ ! -d "/home/${ADDITIONAL_USER}/.ssh" ]; then
			mkdir -p "/home/${ADDITIONAL_USER}/.ssh"
			chown -R $(id -u "${ADDITIONAL_USER}"):$(id -g "${ADDITIONAL_USER}") "/home/${ADDITIONAL_USER}/.ssh"
		fi
		echo "${ADDITIONAL_USER_PUBLICKEY}" >> /home/"${ADDITIONAL_USER}"/.ssh/authorized_keys
	fi
fi
if [[ ! -z "${ROOT_PASSWORD_FILE}" ]]; then
	echo "root:$(cat $ROOT_PASSWORD_FILE)" | chpasswd
fi
if [[ ! -z "${ROOT_PUBLICKEY}" ]]; then
	if [ ! -d "/root/.ssh" ]; then
		mkdir -p "/root/.ssh"
	fi
	echo "${ROOT_PUBLICKEY}" >> /root/.ssh/authorized_keys
fi
if [[ (! -z "${ENABLE_PASSWORD_AUTHENTICATION}") && ("${ENABLE_PASSWORD_AUTHENTICATION}" = '1') ]]; then
	echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config
	if [[ (! -z "${ENABLE_ROOT}") && ("${ENABLE_ROOT}" = 1) ]]; then
		echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
	else
		echo "PermitRootLogin no" >> /etc/ssh/sshd_config
	fi
else
	echo "PasswordAuthentication no" >> /etc/ssh/sshd_config
	if [[ (! -z "${ENABLE_ROOT}") && ("${ENABLE_ROOT}" = '1') ]]; then
		echo "PermitRootLogin without-password" >> /etc/ssh/sshd_config
	else
		echo "PermitRootLogin no" >> /etc/ssh/sshd_config
	fi
fi

rm /etc/ssh/ssh_host_*key*
ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key
ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key
ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key
ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key

usermod -s /bin/bash root
/usr/sbin/sshd -D
#exec "$@"
