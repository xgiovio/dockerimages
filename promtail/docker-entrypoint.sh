#!/bin/bash


if [[ (! -z "${PROMTAIL_DAEMON_USER}" ) && ( "${PROMTAIL_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $PROMTAIL_DAEMON_USER
	if [[ ! -z "${PROMTAIL_DAEMON_USER_UID}" ]]; then
		usermod -u $PROMTAIL_DAEMON_USER_UID $PROMTAIL_DAEMON_USER
	fi
	if [[ ! -z "${PROMTAIL_DAEMON_USER_GID}" ]]; then
		groupmod -g $PROMTAIL_DAEMON_USER_GID $PROMTAIL_DAEMON_USER
	fi
fi

echo "Chowning Data"
if [[ ! -z "${PROMTAIL_DAEMON_USER}" ]]; then
	chown -R $(id -u ${PROMTAIL_DAEMON_USER}):$(id -g ${PROMTAIL_DAEMON_USER}) /promtail
else
	chown -R $(id -u):$(id -g) /promtail
fi


if [[ ! -z "${PROMTAIL_DAEMON_USER}" ]]; then
	runuser -u ${PROMTAIL_DAEMON_USER} -- /promtail/promtail-linux-amd64 -config.file=/promtail/config.yaml
else
	/promtail/promtail-linux-amd64 -config.file=/promtail/config.yaml	
fi
