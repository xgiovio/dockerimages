#!/bin/bash


if [[ (! -z "${NGINX_DAEMON_USER}" ) && ( "${NGINX_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $NGINX_DAEMON_USER
	if [[ ! -z "${NGINX_DAEMON_USER_UID}" ]]; then
		usermod -u $NGINX_DAEMON_USER_UID $NGINX_DAEMON_USER
	fi
	if [[ ! -z "${NGINX_DAEMON_USER_GID}" ]]; then
		groupmod -g $NGINX_DAEMON_USER_GID $NGINX_DAEMON_USER
	fi
fi

cp /etc/nginx/nginx.conf /etc/nginx/nginx_tmp.conf
sed -i "s/user .*;//" /etc/nginx/nginx_tmp.conf

echo "Chowning Data"
if [[ ! -z "${NGINX_DAEMON_USER}" ]]; then
	chown -R $(id -u ${NGINX_DAEMON_USER}):$(id -g ${NGINX_DAEMON_USER}) /usr/share/nginx/html
	chown -R $(id -u ${NGINX_DAEMON_USER}):$(id -g ${NGINX_DAEMON_USER}) /etc/nginx/nginx.conf
	chown -R $(id -u ${NGINX_DAEMON_USER}):$(id -g ${NGINX_DAEMON_USER}) /var/log/nginx

	sed -i "1s/^/user ${NGINX_DAEMON_USER};\n/" /etc/nginx/nginx_tmp.conf

else
	chown -R $(id -u):$(id -g) /usr/share/nginx/html
	chown -R $(id -u):$(id -g) /etc/nginx/nginx.conf
	chown -R $(id -u):$(id -g) /var/log/nginx

	sed -i "1s/^/user nginx;\n/" /etc/nginx/nginx_tmp.conf
	
fi

if [[ (! -z "${HOSTNAMESTOCHECK}" ) ]]; then
	"$@" -c "/etc/nginx/nginx_tmp.conf" -g "daemon off;" &

	declare -A HOSTNAMESIP
	IFS="," read -a HOSTNAMESTOCHECKARRAY <<< "${HOSTNAMESTOCHECK}"
	for HOST in "${HOSTNAMESTOCHECKARRAY[@]}"
	do
		HOSTIP=`dig +short $HOST`
		HOSTIP=`echo "$HOSTIP" | tr ' ' '\n' | sort | tr '\n' ' '`
		HOSTNAMESIP[$HOST]=$HOSTIP
		echo IP FOR $HOST $HOSTIP
	done

	sleep 10
	
	while true
	do
		IPCHANGED=0
		for HOST in "${HOSTNAMESTOCHECKARRAY[@]}"
		do
			NEWHOSTIP=`dig +short $HOST`
			NEWHOSTIP=`echo "$NEWHOSTIP" | tr ' ' '\n' | sort | tr '\n' ' '`
			OLDHOSTIP=${HOSTNAMESIP[$HOST]}
			if [ "$NEWHOSTIP" != "$OLDHOSTIP" ]; then
				if [ "$IPCHANGED" = 0 ]; then
					IPCHANGED=1
				fi
				HOSTNAMESIP[$HOST]=$NEWHOSTIP
				echo NEW IP FOR $HOST $NEWHOSTIP
			fi
		done
		if [ "$IPCHANGED" = 1 ]; then
			if [[ `ps -acx|grep nginx|wc -l` > 0 ]]; then
				echo "RELOADING NGINX"
				nginx -s reload
			else
				echo "RESTARTING NGINX"
				"$@" -c "/etc/nginx/nginx_tmp.conf" -g "daemon off;" &
			fi
		fi
		sleep 10
	done

else

	"$@" -c "/etc/nginx/nginx_tmp.conf" -g "daemon off;"

fi







