#!/bin/bash


if [[ (! -z "${MYSQL_DAEMON_USER}" ) && ( "${MYSQL_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $MYSQL_DAEMON_USER
	if [[ ! -z "${MYSQL_DAEMON_USER_UID}" ]]; then
		usermod -u $MYSQL_DAEMON_USER_UID $MYSQL_DAEMON_USER
	fi
	if [[ ! -z "${MYSQL_DAEMON_USER_GID}" ]]; then
		groupmod -g $MYSQL_DAEMON_USER_GID $MYSQL_DAEMON_USER
	fi
fi

if [ ! -d "/usr/mysql-cluster" ]; then
	mkdir -p /usr/mysql-cluster
fi

if [ ! -d "/var/lib/mysql" ]; then
	mkdir -p /var/lib/mysql
fi

if [ ! -d "/var/run/mysqld" ]; then
	mkdir -p /var/run/mysqld
fi

if [[ ( "${MYSQLD_CLUSTER}" = "true"  ) ]]; then
	DBEXIST=1
	if [ ! -d "/var/lib/mysql/mysql" ]; then
		DBEXIST=0
		echo "Creating mysqld system tables"
		mysqld --initialize-insecure
	fi
fi

echo "Chowning Data"
if [[ ! -z "${MYSQL_DAEMON_USER}" ]]; then
	chown -R $(id -u ${MYSQL_DAEMON_USER}):$(id -g ${MYSQL_DAEMON_USER}) /var/lib/mysql
	chown -R $(id -u ${MYSQL_DAEMON_USER}):$(id -g ${MYSQL_DAEMON_USER}) /usr/mysql-cluster
	chown -R $(id -u ${MYSQL_DAEMON_USER}):$(id -g ${MYSQL_DAEMON_USER}) /var/run/mysqld
else
	chown -R $(id -u):$(id -g) /var/lib/mysql
	chown -R $(id -u):$(id -g) /usr/mysql-cluster
	chown -R $(id -u):$(id -g) /var/run/mysqld
fi

start_process () {
	if [[ ! -z "${MYSQL_DAEMON_USER}" ]]; then
		if [[ ( "${MYSQLD_CLUSTER}" = "true"  ) ]]; then
			runuser -u ${MYSQL_DAEMON_USER} -- $@ --socket=/tmp/mysql.sock --bind-address=0.0.0.0 &
		else
			runuser -u ${MYSQL_DAEMON_USER} -- $@ &
		fi
	else
		if [[ ( "${MYSQLD_CLUSTER}" = "true"  ) ]]; then
			sed -i "s/.*user=.*/user=root/" /etc/my.cnf #needed to change the user option otherwise mysqld doesn't start in root mode
			$@ --user=root --socket=/tmp/mysql.sock -bind-address=0.0.0.0 &
		else
			$@ &
		fi	
	fi
}
echo "Starting process. Sleeping 15 sec before continuing"	
start_process $@
sleep 15

if [[ -z "${MYSQLD_CLUSTER_NOPASS}" ]]; then
	MYSQLD_CLUSTER_NOPASS="false"
fi

if [[ ($DBEXIST -eq 0) && ( "${MYSQLD_CLUSTER}" = "true"  ) &&  ( "${MYSQLD_CLUSTER_NOPASS}" = "false"  )]]; then

	echo "Trying to set DB Root Password"
	MYSQLSTARTED=0
	FIRSTPHASE=0;
	SECONDPHASE=0;

	PASSWORD="default"

	if [[ ! -z "${MYSQLD_ROOT_PASSWORD}" ]]; then
		PASSWORD="${MYSQLD_ROOT_PASSWORD}"
	fi

	if [[ ! -z "${MYSQLD_ROOT_PASSWORD_FILE}" ]]; then
		PASSWORD="$(cat ${MYSQLD_ROOT_PASSWORD_FILE})"
	fi

	while [[ $MYSQLSTARTED -eq 0 ]]
	do
		#mysqld could not start because config need to be confirmed by management servers
		while [[ -z $(ps -e | grep mysqld)  ]]
		do
			echo "Starting process again. Sleeping 15 sec before continuing"	
			start_process $@
			sleep 15
		done

		RESULT=""

		mysql -u root -e "CREATE USER 'root'@'%' IDENTIFIED BY '${PASSWORD}';GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';" --socket=/tmp/mysql.sock
		if [ $? -ne 0 ]; then
			RESULT=$(mysql --socket=/tmp/mysql.sock -u root -e "SELECT 1 FROM mysql.user WHERE user = 'root' AND host = '%';")
			if [[ (! -z "${RESULT}") && ( $? -eq 0 ) ]]; then
				mysql -u root -e "ALTER USER 'root'@'%' IDENTIFIED BY '${PASSWORD}';" --socket=/tmp/mysql.sock
				if [ $? -eq 0 ]; then
					FIRSTPHASE=1;
				fi
			fi
		else
			FIRSTPHASE=1;
		fi
		
		if [[ $FIRSTPHASE -eq 1 ]]; then
			mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '${PASSWORD}';FLUSH PRIVILEGES;" --socket=/tmp/mysql.sock
			if [ $? -eq 0 ]; then
				SECONDPHASE=1;
			fi
		fi


		if [[ ( $FIRSTPHASE -eq 1  ) &&  ( $SECONDPHASE -eq 1  )]]; then
		   	MYSQLSTARTED=1
		   	echo "DB Root Password set"
		else
		   	echo "Retrying to set DB Root Password in 10 sec"
			sleep 10
		fi
	done
fi

wait



