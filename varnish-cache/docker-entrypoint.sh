#!/bin/bash


if [[ (! -z "${VARNISH_DAEMON_USER}" ) && ( "${VARNISH_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $VARNISH_DAEMON_USER
	if [[ ! -z "${VARNISH_DAEMON_USER_UID}" ]]; then
		usermod -u $VARNISH_DAEMON_USER_UID $VARNISH_DAEMON_USER
	fi
	if [[ ! -z "${VARNISH_DAEMON_USER_GID}" ]]; then
		groupmod -g $VARNISH_DAEMON_USER_GID $VARNISH_DAEMON_USER
	fi
fi

if [[ ! -z "${VARNISH_DAEMON_USER}" ]]; then
	chown -R $(id -u ${VARNISH_DAEMON_USER}):$(id -g ${VARNISH_DAEMON_USER}) /var/lib/varnish
	setcap 'cap_net_bind_service=+ep' /usr/sbin/varnishd
fi

if [[ (! -z "${HOSTNAMESTOCHECK}" ) ]]; then
	if [[ ! -z "${VARNISH_DAEMON_USER}" ]]; then
		runuser -u ${VARNISH_DAEMON_USER} -- varnishd -F -T localhost:6082 -f /etc/varnish/default.vcl -S /run/secrets/varnish_password "$@" &
	else
		varnishd -F -T localhost:6082 -f /etc/varnish/default.vcl -S /run/secrets/varnish_password "$@" &
	fi

	declare -A HOSTNAMESIP
	IFS="," read -a HOSTNAMESTOCHECKARRAY <<< "${HOSTNAMESTOCHECK}"
	for HOST in "${HOSTNAMESTOCHECKARRAY[@]}"
	do
		HOSTIP=`dig +short $HOST`
		HOSTIP=`echo "$HOSTIP" | tr ' ' '\n' | sort | tr '\n' ' '`
		HOSTNAMESIP[$HOST]=$HOSTIP
		echo IP FOR $HOST $HOSTIP
	done

	sleep 10
	
	while true
	do
		IPCHANGED=0
		for HOST in "${HOSTNAMESTOCHECKARRAY[@]}"
		do
			NEWHOSTIP=`dig +short $HOST`
			NEWHOSTIP=`echo "$NEWHOSTIP" | tr ' ' '\n' | sort | tr '\n' ' '`
			OLDHOSTIP=${HOSTNAMESIP[$HOST]}
			if [ "$NEWHOSTIP" != "$OLDHOSTIP" ]; then
				if [ "$IPCHANGED" = 0 ]; then
					IPCHANGED=1
				fi
				HOSTNAMESIP[$HOST]=$NEWHOSTIP
				echo NEW IP FOR $HOST $NEWHOSTIP
			fi
		done
		if [ "$IPCHANGED" = 1 ]; then
			if [[ `ps -acx|grep varnishd|wc -l` > 0 ]]; then
				echo "RELOADING VARNISH"
				TIME=$(date +%s)
				varnishadm -S /run/secrets/varnish_password -T localhost:6082 vcl.load varnish_$TIME /etc/varnish/default.vcl
				varnishadm -S /run/secrets/varnish_password -T localhost:6082 vcl.use varnish_$TIME
			else
				echo "RESTARTING VARNISH"
				if [[ ! -z "${VARNISH_DAEMON_USER}" ]]; then
					runuser -u ${VARNISH_DAEMON_USER} -- varnishd -F -T localhost:6082 -f /etc/varnish/default.vcl -S /run/secrets/varnish_password "$@" &
				else
					varnishd -F -T localhost:6082 -f /etc/varnish/default.vcl -S /run/secrets/varnish_password "$@" &
				fi
			fi
		fi
		sleep 10
	done

else

	if [[ ! -z "${VARNISH_DAEMON_USER}" ]]; then
		runuser -u ${VARNISH_DAEMON_USER} -- varnishd -F -T localhost:6082 -f /etc/varnish/default.vcl -S /run/secrets/varnish_password "$@"
	else
		varnishd -F -T localhost:6082 -f /etc/varnish/default.vcl -S /run/secrets/varnish_password "$@"
	fi

fi






