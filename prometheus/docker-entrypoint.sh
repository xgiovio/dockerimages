#!/bin/bash


if [[ (! -z "${PROMETHEUS_DAEMON_USER}" ) && ( "${PROMETHEUS_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $PROMETHEUS_DAEMON_USER
	if [[ ! -z "${PROMETHEUS_DAEMON_USER_UID}" ]]; then
		usermod -u $PROMETHEUS_DAEMON_USER_UID $PROMETHEUS_DAEMON_USER
	fi
	if [[ ! -z "${PROMETHEUS_DAEMON_USER_GID}" ]]; then
		groupmod -g $PROMETHEUS_DAEMON_USER_GID $PROMETHEUS_DAEMON_USER
	fi
fi

echo "Chowning Data"
if [[ ! -z "${PROMETHEUS_DAEMON_USER}" ]]; then
	chown -R $(id -u ${PROMETHEUS_DAEMON_USER}):$(id -g ${PROMETHEUS_DAEMON_USER}) /prometheus
else
	chown -R $(id -u):$(id -g) /prometheus
fi


if [[ ! -z "${PROMETHEUS_DAEMON_USER}" ]]; then
	runuser -u ${PROMETHEUS_DAEMON_USER} -- $@
else
	$@	
fi
