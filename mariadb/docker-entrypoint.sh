#!/bin/bash


if [[ (! -z "${MYSQL_DAEMON_USER}" ) && ( "${MYSQL_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $MYSQL_DAEMON_USER
	if [[ ! -z "${MYSQL_DAEMON_USER_UID}" ]]; then
		usermod -u $MYSQL_DAEMON_USER_UID $MYSQL_DAEMON_USER
	fi
	if [[ ! -z "${MYSQL_DAEMON_USER_GID}" ]]; then
		groupmod -g $MYSQL_DAEMON_USER_GID $MYSQL_DAEMON_USER
	fi
fi

DBEXIST=1
if [ ! -d "/var/lib/mysql/mysql" ]; then
	DBEXIST=0
	echo "Creating system tables"
	if [[ ! -z "${MYSQL_DAEMON_USER}" ]]; then
		mysql_install_db --user=$MYSQL_DAEMON_USER
	else
		mysql_install_db
	fi

else
	echo "Skipping system tables creation. Chowning Data"
	if [[ ! -z "${MYSQL_DAEMON_USER}" ]]; then
		chown -R $(id -u ${MYSQL_DAEMON_USER}):$(id -g ${MYSQL_DAEMON_USER}) /var/lib/mysql
	else
		chown -R $(id -u):$(id -g) /var/lib/mysql
	fi
	
fi

if [[ ! -z "${MYSQL_DAEMON_USER}" ]]; then
	runuser -u ${MYSQL_DAEMON_USER} -- $@ --socket=/tmp/mysql.sock --bind-address=0.0.0.0 &
else
	$@ --user=root --socket=/tmp/mysql.sock -bind-address=0.0.0.0 &
fi


if [[ $DBEXIST -eq 0 ]]; then

	MYSQLSTARTED=0

	while [[ $MYSQLSTARTED -eq 0 ]]
	do
		if [[ ! -z "${MYSQL_ROOT_PASSWORD}" ]]; then
			mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}'" --socket=/tmp/mysql.sock
		else
			mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'default'" --socket=/tmp/mysql.sock
		fi

		if [ $? -eq 0 ]; then
		   	MYSQLSTARTED=1
		   	echo "DB Root Password set"
		else
		   	echo "Trying to set DB Root Password"
			sleep 3
		fi
	done
else
	echo "Skipping DB Root Password setting"
fi

wait


