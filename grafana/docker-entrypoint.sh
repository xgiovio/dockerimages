#!/bin/bash


if [[ (! -z "${GRAFANA_DAEMON_USER}" ) && ( "${GRAFANA_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $GRAFANA_DAEMON_USER
	if [[ ! -z "${GRAFANA_DAEMON_USER_UID}" ]]; then
		usermod -u $GRAFANA_DAEMON_USER_UID $GRAFANA_DAEMON_USER
	fi
	if [[ ! -z "${GRAFANA_DAEMON_USER_GID}" ]]; then
		groupmod -g $GRAFANA_DAEMON_USER_GID $GRAFANA_DAEMON_USER
	fi
fi

if [ ! -d "/grafana/data" ]; then
	mkdir -p /grafana/data
fi

if [ ! -d "/grafana/logs" ]; then
	mkdir -p /grafana/logs
fi

if [ ! -d "/grafana/plugins" ]; then
	mkdir -p /grafana/plugins
fi

if [ ! -d "/grafana/provisioning" ]; then
	mkdir -p /grafana/provisioning
fi

echo "Chowning Data"
if [[ ! -z "${GRAFANA_DAEMON_USER}" ]]; then
	chown -R $(id -u ${GRAFANA_DAEMON_USER}):$(id -g ${GRAFANA_DAEMON_USER}) /grafana
	chown -R $(id -u ${GRAFANA_DAEMON_USER}):$(id -g ${GRAFANA_DAEMON_USER}) /usr/share/grafana
	
else
	chown -R $(id -u):$(id -g) /grafana
	chown -R $(id -u):$(id -g) /usr/share/grafana
fi


if [[ ! -z "${GRAFANA_DAEMON_USER}" ]]; then
	runuser -u ${GRAFANA_DAEMON_USER} -- /usr/sbin/grafana-server --homepath /usr/share/grafana --config /etc/grafana/grafana.ini cfg:default.paths.logs=/grafana/logs cfg:default.paths.data=/grafana/data cfg:default.paths.plugins=/grafana/plugins cfg:default.paths.provisioning=/grafana/provisioning
else
	/usr/sbin/grafana-server --homepath /usr/share/grafana --config /etc/grafana/grafana.ini cfg:default.paths.logs=/grafana/logs cfg:default.paths.data=/grafana/data cfg:default.paths.plugins=/grafana/plugins cfg:default.paths.provisioning=/grafana/provisioning	
fi
