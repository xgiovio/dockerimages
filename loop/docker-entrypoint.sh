#!/bin/bash


if [[ (! -z "${LOOP_DAEMON_USER}" ) && ( "${LOOP_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $LOOP_DAEMON_USER
	if [[ ! -z "${LOOP_DAEMON_USER_UID}" ]]; then
		usermod -u $LOOP_DAEMON_USER_UID $LOOP_DAEMON_USER
	fi
	if [[ ! -z "${LOOP_DAEMON_USER_GID}" ]]; then
		groupmod -g $LOOP_DAEMON_USER_GID $LOOP_DAEMON_USER
	fi
fi

if [[ ! -z "${LOOP}" ]]; then
	while :
	do
		if [[ ! -z "${LOOP_DAEMON_USER}" ]]; then
			runuser -u ${LOOP_DAEMON_USER} -- "$@"
		else
			"$@"
		fi
		sleep "${LOOP}"
	done
else
	if [[ ! -z "${LOOP_DAEMON_USER}" ]]; then
		runuser -u ${LOOP_DAEMON_USER} -- "$@"
	else
		"$@"
	fi
fi




