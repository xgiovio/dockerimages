#!/bin/bash


if [[ (! -z "${PHP_DAEMON_USER}" ) && ( "${PHP_DAEMON_USER}" != "root"  ) ]]; then
	useradd -r -s /bin/false $PHP_DAEMON_USER
	if [[ ! -z "${PHP_DAEMON_USER_UID}" ]]; then
		usermod -u $PHP_DAEMON_USER_UID $PHP_DAEMON_USER
	fi
	if [[ ! -z "${PHP_DAEMON_USER_GID}" ]]; then
		groupmod -g $PHP_DAEMON_USER_GID $PHP_DAEMON_USER
	fi
fi

if [ ! -d "/var/opt/remi/php80/lib/php/session" ]; then
	mkdir -p /var/opt/remi/php80/lib/php/session
fi

cp /etc/opt/remi/php80/php.ini /etc/opt/remi/php80/php-tmp.ini
cp /etc/opt/remi/php80/php-fpm.conf /etc/opt/remi/php80/php-fpm-tmp.conf  

if [[ ! -z "${PHP_DAEMON_USER}" ]]; then
	sed -i "s/^user = .*/user = ${PHP_DAEMON_USER}/" /etc/opt/remi/php80/php-fpm-tmp.conf
	sed -i "s/^group = .*/group = ${PHP_DAEMON_USER}/" /etc/opt/remi/php80/php-fpm-tmp.conf
	chown -R $(id -u ${PHP_DAEMON_USER}):$(id -g ${PHP_DAEMON_USER}) /var/opt/remi/php80/lib/php/session
	 
fi
	
"$@" "--nodaemonize" --fpm-config /etc/opt/remi/php80/php-fpm-tmp.conf --php-ini /etc/opt/remi/php80/php-tmp.ini 




